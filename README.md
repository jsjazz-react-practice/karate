# KarateNotes
*GA SEI Capstone Project, SEI726 2021*

KarateNotes is note taking app boilerplate. It is fundamentally a place to enter and retreive bits of text or code, with a very simple deoployment.  
  
As a capstone project to a software intensive program, I wanted something that could grow with me as I learned while being a useful tool where I could be familiar with 100% of the API. 

## Why a notes app?
Does the world really need another notes app? Not really. There are many amazing apps available in this genre, but most of them on the market come with a series of compromises. 
- Licensing or subscription models. 
- Data access. 
- Privacy concerns. 
- Feature bloat. 
- A UI that nests data so deep that it becomes forgotten. 
- So much customization options that it becomes distracting. 
  
Modern trends in PKM (Personal Knowledge Management) applications have been a big inspiration for me to explore coding and data management. And if only for this reason, taking a stab at making a basic PKM tool myself felt like nice way to capstone my first steps into professional development. 

## Project Goals
- Very minimal and intuitive UI. 
    - This is done in purpose with the intent to opening the app and entering data with no more than a few clicks. 
- Modular UI framework.
    - This boilerplate is meant to be upgraded. With minimal effort, switching between bootstrap, material, atomic, or completely custom styles should not break the core applicaiton functions. 
- Single repository deployment. 
    - There are too many good hosting options to not take advantage of them. While the initial release uses firebase and firestore hooks, it should not be too dificult to set up a monorepo and custom express server if desired. Or choose from a variety of services like firebase or netlify to deploy straight from a GitHub or GitLab repository. 
- Extensibility.
    - There are a variety of stretch-goals to follow in subsequent upgrades detailed in the following. 

# Initial Release
As a foundational project for my own continuing education, I will be focussing on my own engineering choices. 

## Technologies Used
React JS  
Node + Yarn  
React-Bootstrap  
UUIDv4  
CodeMirror  
Firebase + Firestor  
GitLab  
Netflify  
VSCode + SublimeText  
Cascadia Code & Roboto Fonts

# Credits
Links and sources of code and inspiration  

## Inspiration
[stackedit.io](https://stackedit.io/)  
[excalidraw](https://excalidraw.com)  
[obsidian.md](https://obsidian.md)  
[vscode](https://code.visualstudio.com)  
[boostnote](https://boostnote.io)  
[apple notes](https://www.apple.com)  
[notion](https://www.notion.so)  

## Code & Guides
### Walkthroughs
intesnive program from: https://generalassemb.ly  
James Grimshaw: https://youtu.be/ulOKYl5sHGk (used for initial site & wireframe tests)  
https://www.youtube.com/watch?v=3qnrfkeguXg (serviceWorker base)  
 - https://github.com/portexe/evernote-clone/blob/master/src/serviceWorker.js  

  
### firebase
https://www.youtube.com/watch?v=zQyrwxMPm88 Fireship, react+firebase intro  
https://itnext.io/integrate-react-with-firebase-and-deploying-with-gitlab-netlify-8b47654c70bb)  
https://firebase.google.com/docs/web/setup  

## netifly frontend deployment
https://app.netlify.com/teams/juliajazz/overview  
pre-prod fe host https://app.netlify.com/sites/lucid-wescoff-80d099/overview  

  
## Styling
mia+oddbird https://www.oddbird.net/cascading-colors/docs/_  
css.irl https://css-irl.info/quick-and-easy-dark-mode-with-css-custom-properties/  
structure https://thoughtbot.com/blog/structure-for-styling-in-react-native  
mui https://mui.com/getting-started/installation/ & react-bootstrap  

## Logo "Kata"
https://jisho.org/search/方%20%23kanji


# Interesting Snippets

**firebase auth template**
```jsx
import firebase from "firebase/compat/app";
import "firebase/compat/firestore";
import "firebase/auth";


const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_APIKEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTHDOMAN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECTID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGINGSENDERID,
  appId: process.env.REACT_APP_FIREBASE_APPID,
  measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENTID,
};
firebase.initializeApp(firebaseConfig);

export default firebase;
```

**firestore onSnapshot**
https://firebase.google.com/docs/firestore/query-data/listen
```jsx
import firebase from './firebase'
//...
  const ref = firebase.firestore().collection('notes');
  function getNotes() {
    setLoading(true);
    ref
      .onSnapshot((querySnapshot) => {
        const items = [];
        querySnapshot.forEach((doc) => {
          items.push(doc.data());
        });
        setNotes(items);
        setLoading(false);
      });}
  useEffect(() => {
    getNotes(); // eslint-disable-next-line
  }, []);
```

**CodeMirror Config**
*NYI* [CodeMirror Config Notes](docs/s-cmirror.md)

