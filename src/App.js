import React from 'react';
import { useState, useEffect } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import firebase from './firebase'
import { v4 as uuidv4 } from 'uuid';
// import {doc, deleteDoc} from 'firebase/firestore'
// import {getDoc, query, where} from 'firebase/firestore'
import Header from './components/Header';
import NoteList from './components/NoteList';
// import NoteView from './components/NoteView'; // TODO: component merged for testing
import { Form, Button } from 'react-bootstrap'
import "fontsource-cascadia-code/"


function App() {
  const [notes, setNotes] = useState([]);
  const [loading, setLoading] = useState(false);
  // "ref" the notes database in firestore:
  const ref = firebase.firestore().collection('notes');
  const [activeNote, setActiveNote] = useState({});
  

  // ====================================================================== GET OK!
  function getNotes() {
    setLoading(true);
    ref
      .onSnapshot((querySnapshot) => {
        const items = [];
        querySnapshot.forEach((doc) => {
          items.push(doc.data());
        });
        setNotes(items);
        setLoading(false);
      });}
  useEffect(() => {
    getNotes(); // eslint-disable-next-line
  }, []);

  // ====================================================================== ADD OK!
  function addNote() {
    const newNote = {
      title: "Title: new note ",
      body: "Note Text",
      lastUpdate: Date.now(),
      uid: uuidv4(),
    };
    ref.doc(newNote.id).set(newNote).catch((err) => {console.error(err);});
    // setActiveNote(newNote.uid);
  };
  // ====================================================================== DEL OK!
  const deleteNote = async (id) => {
    ref.where("uid", "==", id).get()
      .then(querySnapshot => {
          querySnapshot.docs[0].ref.delete();
      });
  }
  
  // ====================================================================== Get/Edit
  // TODO: working...
  // eslint-disable-next-line
  const getActiveNote = () => {
    console.log(activeNote)
    return notes.find(({ id }) => id === activeNote)
  };

  // ====================================================================== EDIT
  // TODO: working...

  // const handleFieldUpdate = (event) => {
  //   const { target } = event
  //   const value = target.type === 'checkbox' ? +(target.checked) : target.value
  //   const { name } = target
  //   setNewClip((oldClip) => ({ ...oldClip, [ name ]: value }))
  //   setSearch((name === 'location') ? value : '')
  //   if (target.name === 'aggregator') {
  //     handleSubmitAggregator()
  //   }
  // }

  const [activeNoteTitle, setActiveNoteTitle] = useState("");

  const handleNoteEdit = (event) => {
    event.preventDefault()
    const {target} = event
    const value = target.value
    // const name = target
    console.log(value)
    setActiveNoteTitle(value)
    console.log(activeNote)

    // send to db handleSubmit
  }
  
  const editNote = (field, value) => {
    console.log(value)
    };

  // TODO: working... ------------------------------------------- NoteView re-import
  // if (!activeNote) return <div className="no-active-note">No Active Note</div>
  function NoteView() {
    if (!activeNote) return <div className="no-active-note">No Active Note</div>;
    return (
      <div className='box main'>
  
        <Form >
          <Button variant="dark" type="submit">Save</Button>
          <Form.Control 
            type="text"
            id="note-title" 
            placeholder="Note Title"
            onChange={handleNoteEdit}
            // onChange={(e) => onEditField("title", e.preventDefault())}
            value={activeNoteTitle}
            autoFocus
            > 
          </Form.Control>
  
          <Form.Control 
            as="textarea"
            size="sm"
            style={{ resize:"none", fontFamily:`"Cascadia Code"` }}
            id="note-body" 
            placeholder="Put your note content here..."
            onSubmit={(e) => editNote("note-body", e.target.value)}
            // onChange={(e) => onEditField("body", e.preventDefault())}
            value={activeNote.body}
            autoFocus
            >
          </Form.Control>
        </Form>
      </div>
    )
  }

  return (
    <div className="App wrapper">
      <Header/>
      <NoteList 
        notes={notes}
        addNote={addNote}
        loading={loading}
        activeNote={activeNote}
        setActiveNote={setActiveNote}
        deleteNote={deleteNote}
      />
      <NoteView/>
      {/* <NoteView
        activeNote={activeNote}
        // activeNote={getActiveNote()}
        editNote={editNote}
        // onUpdateNote={onUpdateNote}
      /> */}
    </div>
  )
}

export default App;
