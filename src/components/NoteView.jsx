import React from 'react'
// import { useState } from 'react';
import { Form, Button } from 'react-bootstrap'
import "fontsource-cascadia-code/"



function NoteView({ activeNote, editNote, onUpdateNote }) {


  if (!activeNote) return <div className="no-active-note">No Active Note</div>;

  return (
    <div className='box main'>

      <Form>
        <Button variant="dark" type="submit">Save</Button>
        <Form.Control 
          type="text" 
          id="note-title" 
          placeholder="Note Title"
          onSubmit={(e) => editNote("note-title", e.target.value)}
          // onChange={(e) => onEditField("title", e.preventDefault())}
          value={activeNote.title}
          autoFocus
          > 
        </Form.Control>

        <Form.Control 
          as="textarea"
          size="sm"
          style={{ resize:"none", fontFamily:`"Cascadia Code"` }}
          id="note-body" 
          placeholder="Put your note content here..."
          onSubmit={(e) => editNote("note-body", e.target.value)}
          // onChange={(e) => onEditField("body", e.preventDefault())}
          value={activeNote.body}
          autoFocus
          >
        </Form.Control>
      </Form>
    </div>
  )
}

export default NoteView
