# Imports
https://codemirror.net/doc/manual.html#styling

`yarn add react-codemirror2 codemirror --save`

```jsx
import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/ayu-mirage.css'
import 'codemirror/mode/xml/xml'
import 'codemirror/mode/jsx/jsx'
```

## Options
- lineWrapping (def is false)
- lineNumbers
- tabinxex (??? indents?)
- tabSize (def = 4)
- theme
- mode: language (might )
- addon - https://codemirror.net/addon/edit/matchbrackets.js
- addon - https://codemirror.net/addon/edit/closebrackets.js
- addon - https://codemirror.net/addon/edit/matchtags.js
- addon - https://codemirror.net/addon/edit/closetag.js
- addon - https://codemirror.net/addon/edit/trailingspace.js
- addon - https://codemirror.net/addon/comment/comment.js
    - check options...
- addon - https://codemirror.net/addon/fold/foldcode.js (or foldgutter)

## Language Modes
- CSS
- Go
- HTML-mixed (XML?)
- JSX (Javascript)
- Markdown
- Java
- C-Like
- Kotlin
- YAML
- PHP
- Python
- SCSS
- SQL
- JSON-LD
- Shell
- reStructuredText (plain text)




# Codemirror Basic App + Component

App.js
```jsx
import Editor from './Editor'
import './App.css';
import React, { useState } from 'react';
function App() {
  const [jsx, setJsx] = useState('')
  return (
    <div className="App">
      <Editor language="jsx" displayName="JS" value={jsx} onChange={setJsx}/>
      <iframe
        title = "output"
        sandbox = "allow-scripts" // maybe turn this off as I'm not rendering
        width = "100%"
        height = "100%"
      />
    </div>
  );
}
export default App;
```

```jsx
import React from 'react'
import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/ayu-mirage.css'
import 'codemirror/mode/xml/xml'
import 'codemirror/mode/jsx/jsx'
import { Controlled as ControlledEditor } from 'react-codemirror2'
export default function Editor(props) {
  const {
    language,
    displayName,
    value,
    onChange,
  } = props
  function handleChange(editor, data, value) {
    onChange(value)
  }
  return (
    <div className='editor-container'>
      <div className='editor-title'>
        {displayName}
        <button>O/C</button>
      </div>
      <ControlledEditor
        onBeforeChange={handleChange}
        value={value}
        className="code-mirror-wrapper"
        options={{
          lineWrapping: true,
          lint: true,
          mode: language,
          theme: 'ayu-mirage',
          lineNumbers: true,
        }}
      />
    </div>
  )
}
```