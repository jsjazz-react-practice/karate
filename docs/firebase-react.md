create react app

# install firebase, react-firebase-hooks

--- in firebase app settings
- enable google signin
- firestore -> enable database
    - project publicfacing name: 
- gear -> add new web app
    - appid: 1:200387888851:web:9b3c90444273b2b5bd3626
    - 
    - 
## Copied from firebase
```jsx
// copied from firebase
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCvuLgK2J0pJAeySXlDnVMAh4vSZcksOTM",
  authDomain: "karatenotes.firebaseapp.com",
  projectId: "karatenotes",
  storageBucket: "karatenotes.appspot.com",
  messagingSenderId: "200387888851",
  appId: "1:200387888851:web:9b3c90444273b2b5bd3626",
  measurementId: "G-FY4EX84720"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
```
## App config
```jsx
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
// hooks
import { useAuthState } from 'react-firebase-hooks/auth';
import { useCollectionData } from 'react-firebase-hooks/firestore';

// global vars
firebase.initializeApp({
  apiKey: "AIzaSyCvuLgK2J0pJAeySXlDnVMAh4vSZcksOTM",
  authDomain: "karatenotes.firebaseapp.com",
  projectId: "karatenotes",
  storageBucket: "karatenotes.appspot.com",
  messagingSenderId: "200387888851",
  appId: "1:200387888851:web:9b3c90444273b2b5bd3626",
  measurementId: "G-FY4EX84720"
})
const auth = firebase.auth();
const firestore = firebase.firestore();

const [user] = useAuthState(auth);
```

## signin-out state define in app.js

```jsx
//later in return
// if user is defined, show someThing, otherwise show SignIn //
<section> {user ? <someThing/> : <SignIn/>}
```

## Sign-in Component
```jsx
function SignIn() {
    // signin fn
    const signInWithGoogle = () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        auth.signInWithPopup(provider);
    }
    // signout fn
    function SignOut() {
        return auth.currentUser && (
            <button onClick={() => auth.signOut()}>Sign Out</button>
        )
    }
    return (
    <button onClick={signInWithGoogle}>Sign in With Google</button>
)
}
```

