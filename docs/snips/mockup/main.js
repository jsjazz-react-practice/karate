import ReactMarkdown from "react-markdown";

function Main({activeNote, editNote}){

  const onEditField = (key, value) => {
    editNote({
      // send in an object - reconstruct active note
      ...activeNote,
      [key]: value, // target title or body from key, whichever gets edited
      lastModified: Date.now(),
    })
  };

  if(!(activeNote)) return <div className="no-active-note">Select A Note</div>

  return(
    <div className="app-main">
      <div className="app-main-note-edit">
        <input 
          type="text" 
          id="title" 
          value={activeNote.title} 
          onChange={(e) => onEditField("title", e.target.value)}
          autoFocus />
        <textarea 
          id="body" 
          placeholder="Your note goes here..."
          value={activeNote.body}
          onChange={(e) => onEditField("body", e.target.value)}
          />
      </div>
      {/* <div className="app-main-note-preview">
        <h1 className="preview-title">{activeNote.title}</h1>
        <ReactMarkdown className="markdown-preview">{activeNote.body}</ReactMarkdown> 
      </div> */}
    </div>
  )
}

export default Main;