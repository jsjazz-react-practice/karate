// use to reduce db update calls...
// source: https://www.youtube.com/watch?v=3qnrfkeguXg
// debounce function...
// ref https://dmitripavlutin.com/react-throttle-debounce/
// https://www.npmjs.com/package/lodash.debounce
// https://www.geeksforgeeks.org/lodash-_-debounce-method/


export default function debounce(a,b,c){
  var d,e;
  return function(){
    function h(){
      d=null;
      c||(e=a.apply(f,g));
    }
    var f=this,g=arguments;
    return (clearTimeout(d),d=setTimeout(h,b),c&&!d&&(e=a.apply(f,g)),e)
  }
}

export function removeHTMLTags (str) {
  return str.replace(/<[^>]*>?/gm, '');
};