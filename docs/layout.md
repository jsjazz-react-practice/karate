
With help from mia <3>
https://codepen.io/joulesoix/pen/LYjEbLm?editors=1100

# Init HTML example
```html
<div class="wrapper">
  <div class="box header">Header</div>
  <div class="box menu">Menu</div>
  <div class="box list">List of notes or tags</div>
  <div class="box main">Main</div>
</div>
```

# Init CSS
```css

/*  */
* { box-sizing: border-box; }

html {
  height: 100%;
}

body {
/*   font-size defaults to 16 */
/*   font-size: 16; */
/*   width defaults to 100% */
/*   width: 100%;  */
/* default margins were causing overflow   */
  margin: 0;
/* can use padding if we want that space   */
  padding: 0.1em;
  min-height: 100%;
  overflow: hidden;
/* just to make full-height alignment work */
  display: grid;
}

.header{
  grid-area: header;
/* removed all height calculations   */
}
.menu{
  grid-area: menu;
}
.list{
  grid-area: list;
}
.main {
  grid-area: main;
}

.wrapper {
  display: grid;
/* provide logic for grid layout */
/* "min-content" shrinks to the content, with line breaks wherever possible  */
/* "auto" allows a column or row to size based on content  */
/*  "min()" will use the smaller of two values */
/* "fr" units grow to fill available space   */
  grid-template-columns: min-content min(30%, 40ch) 1fr;
  grid-template-rows: auto 1fr;
  grid-template-areas:
    "menu list header"
    "menu list main";
/* removed the unused footer row   */
  grid-gap: .1em;
}



.box {
  background-color: lightblue;
  border-color: darkblue;
  border-style: solid;
  color: darkblue;
  border-radius: 5px;
  padding: .5em;
  font-size: 1em;
}
```


notes...
/*
Elements:

--Columns--
1. Nav
2. Aside
3. Main
?. Footer

--Nav---- (vertical menu, persistent)
- figure - home
- figure - menu
- figure - list-toggle
- list...
  - list-notes (each)
    - note title
    - note tags
    - note edit date
  - list-tags (each)
    - tag title
    
--Aside-- (list notes/tags, toggle)
- header - list
- nav? - sort (edit-date, )
- nav? - list-notes, list-tags

--Main--- (content, persistent - horiz stretch right->)
- header - main
- header - note
- nav - note-pages
- nav - note-page settings (fields: lang-dropdown, note-page title)
- note page (styling toggle with lang-dropdown)
- footer (optional)

--------*/

/*--------

--Semantic Elements--
Tag	Description
<article>	    Defines independent, self-contained content
<aside>	      Defines content aside from the page content
<details>	    Defines additional details that the user can view or hide
<figcaption>	Defines a caption for a <figure> element
<figure>	    Specifies self-contained content, like illustrations, diagrams, photos, code listings, etc.
<footer>	    Defines a footer for a document or section
<header>	    Specifies a header for a document or section
<main>	      Specifies the main content of a document
<mark>	      Defines marked/highlighted text
<nav>	        Defines navigation links
<section>	    Defines a section in a document
<summary>	    Defines a visible heading for a <details> element
<time>	      Defines a date/time
--------

*/