# capstone-karate
GA capstone project.

> Karate: "empty hand"
> I wanted to start with a nice theme for a bit of inspiration.
> The app will be named "karate" while in development and possibly going forward. 


I want to do this with a mono repo...

::refs::
https://thedeployguy.com/2020-05-10-monorepo-introduction/
https://jibin.tech/monorepo-with-create-react-app/

::nx mono::
https://auth0.com/blog/streamlining-a-react-monorepo/
https://nx.dev/

... but likely just going with firebase for backend config
https://itnext.io/integrate-react-with-firebase-and-deploying-with-gitlab-netlify-8b47654c70bb

