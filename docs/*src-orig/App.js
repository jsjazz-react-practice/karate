import React from 'react';
import './App.css';
// import Menu from "./components/Menu";
// import NoteList from "./components/NoteList";
// import Header from "./components/Header";
// import Main from "./components/Main";
// import SignIn from './firebase/SignIn';
import {useState, useEffect} from "react";
import { v4 as uuidv4 } from 'uuid'; // id randomizer! 🤙 do I need with fbase?
// FIREBASE IMPORTS & HOOKS
import firebase from 'firebase/compat/app'; // ref https://exerror.com/attempted-import-error-firebase-app-does-not-contain-a-default-export-imported-as-firebase/
import 'firebase/firestore';
import 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useCollectionData } from 'react-firebase-hooks/firestore'; // firestore TBD
// import { authRef, notesRef, tagsRef, pagesRef, usersRef } from './config/firebase'

// FIREBASE AUTH - GLOBAL
const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_APIKEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTHDOMAN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECTID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGINGSENDERID,
  appId: process.env.REACT_APP_FIREBASE_APPID,
  measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENTID,
};

firebase.initializeApp(firebaseConfig);
const firestore = firebase.firestore(); // TBD



// APP COMPONENT START =====================================================================//
// =========================================================================================//
const App = () => {
  // Firebase user
  const [user] = useAuthState(auth);

  const [notes, setNotes] = useState(
    localStorage.notes ? JSON.parse(localStorage.notes) : []
  );

  // ============================= STORE TO LOCAL STORAGE - not sure if working... but that's fine
  useEffect(() => {
    // store in local storage with each change
    localStorage.setItem("storedNotes", JSON.stringify(notes))
  }, [notes]);

    // display active note state
  const [activeNote, setActiveNote] = useState(false)
    // ============================= ADD NOTE
  const addNote = () => {
  // make a note object
  const newNote = {
    id: uuidv4(),
    title: "",
    body: "",
    lastModified: Date.now(),
  };
    // append to current array with spread
    setNotes([newNote, ...notes]) // add newnote to existing notes
    console.log(newNote)
  }
    // ============================= EDIT NOTE
  const editNote = (editedNote) => {
  // use map to modify the array
  const editedNoteArr = notes.map((note) => {
      if(note.id === activeNote){
        // replace with edited note
        return editedNote
      }
      // otherwise return no edit
      return note
    })
    setNotes(editedNoteArr)
  }
  // ============================= DELETE NOTE
  const deleteNote = (idDelete) => {
    setNotes(notes.filter((note) => note.id !== idDelete ))
  }
  // ============================= NOTE -> MAIN DISPLAY
  const getActiveNote = () => {
    return notes.find((note) => note.id === activeNote );
  }
  
  return (
  <div className="App wrapper">
    <Menu />
    <NoteList 
      notes={notes} 
      addNote={addNote}
      deleteNote={deleteNote}
      activeNote={activeNote}
      setActiveNote={setActiveNote}
    />
    <section> {user ? <SignOut /> : <SignIn />}</section>
    <Main activeNote={getActiveNote()} editNote={editNote} />
  </div>
  );
}
export default App;
// APP COMPONENT END   =====================================================================//
// =========================================================================================//



// SignIn-out COMPONENT START =====================================================================//
// TODO: SignInOut =================================================================================//
const SignIn = () => {
  // signin fn
  const signInWithGoogle = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider);
  }
  return (
    <div className="box header">
    <button onClick={signInWithGoogle}>Sign in With Google</button>
    </div>
  )
}
const SignOut = () => {
  return auth.currentUser && (
    <div className="box header">
      <button className="sign-out" onClick={() => auth.signOut()}>Sign Out</button>
    </div>
  )
}
// SignIn-out COMPONENT END =====================================================================//
// :  =================================================================================//



// NoteList COMPONENT START =====================================================================//
// TODO: NoteList =================================================================================//
const NoteList = ({ notes, addNote, deleteNote, activeNote, setActiveNote }) => { // destructuring props
  const sortedNotes = notes.sort((a,b)=> b.lastModified - a.lastModified)
  return(
    <div className="box list">
      <div className="">
        <h1>📝 notes</h1>
        <button onClick={addNote}>Add</button>
      </div>
      <div className="list-notes">
        {sortedNotes.map((note) => (
          <div className={`list-note ${note.id === activeNote && "active"}`}
          onClick={() => setActiveNote(note.id)}>
          <div className="list-note-title">
            <strong>{note.title}</strong>
            <button onClick={()=>deleteNote(note.id)}>Delete</button>
          </div>
          <p>{note.body && note.body.substr(0, 100) + "..."}.</p>
          <small className="note-meta">{new Date(note.lastModified).toLocaleDateString("en-US", {
            day: "2-digit",
            month: "short",
            year: "numeric",
            hour12: true,
            hour: "numeric",
            minute: "2-digit",
          })}</small>
        </div>
        ))}
      </div>
    </div>
  )
}
// NoteList COMPONENT END =====================================================================//
// :  =================================================================================//



// Menu COMPONENT Start =====================================================================//
// TODO: Start =================================================================================//
const Menu = () => {
  return(
  <div className="box menu">
    {/* <img src={logo} width={50} alt='KarateNotes'/> */}
    <h1>方</h1>
  </div>
  )
}
// Menu COMPONENT END =====================================================================//
// :  =================================================================================//

// Main COMPONENT START =====================================================================//
// TODO: Main  =================================================================================//
const Main = ({activeNote, editNote}) => {
  const onEditField = (key, value) => {
    editNote({
      // send in an object - reconstruct active note
      ...activeNote,
      [key]: value, // target title or body from key, whichever gets edited
      lastModified: Date.now(),
    })
  };
  if(!(activeNote)) return (
    <div className="box main">
      <div className="main-no-active">Select A Note</div>
    </div>
  )

  return(
    <div className="box main">
      <div className="main-edit">
        <input 
          type="text" 
          id="title" 
          value={activeNote.title} 
          onChange={(e) => onEditField("title", e.target.value)}
          autoFocus />
        <textarea 
          id="body" 
          placeholder="Text goes here..."
          value={activeNote.body}
          onChange={(e) => onEditField("body", e.target.value)}
          />
      </div>
    </div>
  )
}
// Main COMPONENT END =====================================================================//
// : Main =================================================================================//