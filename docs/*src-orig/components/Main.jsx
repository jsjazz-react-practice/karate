

const Main = ({activeNote, editNote}) => {

  const onEditField = (key, value) => {
    editNote({
      // send in an object - reconstruct active note
      ...activeNote,
      [key]: value, // target title or body from key, whichever gets edited
      lastModified: Date.now(),
    })
  };

  if(!(activeNote)) return (
    <div className="box main">
      <div className="main-no-active">Select A Note</div>
    </div>
  )

  return(
    <div className="box main">
      <div className="main-edit">
        <input 
          type="text" 
          id="title" 
          value={activeNote.title} 
          onChange={(e) => onEditField("title", e.target.value)}
          autoFocus />
        <textarea 
          id="body" 
          placeholder="Text goes here..."
          value={activeNote.body}
          onChange={(e) => onEditField("body", e.target.value)}
          />
      </div>
    </div>
  )
}

export default Main;